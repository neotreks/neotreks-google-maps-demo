package com.neotreks.googlemap.googlemap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Tile;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.google.android.gms.maps.model.UrlTileProvider;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    static String NEOTREKS_TOKEN_URL = "https://mapserver.neotreks.com/arcgis/tokens/";
    static String NEOTREKS_URL = "https://mapserver.neotreks.com/arcgis/rest/services/NeoTreks/Slope/MapServer/tile/%d/%d/%d?token=%s";
    static String USERNAME = "<your username>";
    static String PASSWORD = "<your password>";

    public static class CanvasTileProvider implements TileProvider {

        static final int TILE_SIZE = 512;
        private TileProvider mTileProvider;

        public CanvasTileProvider(TileProvider tileProvider) {
            mTileProvider = tileProvider;
        }

        @Override
        public Tile getTile(int x, int y, int zoom) {
            byte[] data;
            Bitmap image = getNewBitmap();
            Canvas canvas = new Canvas(image);
            boolean isOk = onDraw(canvas, zoom, x, y);
            data = bitmapToByteArray(image);
            image.recycle();

            if (isOk) {
                Tile tile = new Tile(TILE_SIZE, TILE_SIZE, data);
                return tile;
            } else {
                return mTileProvider.getTile(x, y, zoom);
            }
        }

        Paint paint = new Paint();

        private boolean onDraw(Canvas canvas, int zoom, int x, int y) {
            x = x * 2;
            y = y * 2;
            Tile leftTop = mTileProvider.getTile(x, y, zoom + 1);
            Tile leftBottom = mTileProvider.getTile(x, y + 1, zoom + 1);
            Tile rightTop = mTileProvider.getTile(x + 1, y, zoom + 1);
            Tile rightBottom = mTileProvider.getTile(x + 1, y + 1, zoom + 1);

            if (leftTop == NO_TILE && leftBottom == NO_TILE && rightTop == NO_TILE && rightBottom == NO_TILE) {
                return false;
            }


            Bitmap bitmap;

            if (leftTop != NO_TILE) {
                bitmap = BitmapFactory.decodeByteArray(leftTop.data, 0, leftTop.data.length);
                canvas.drawBitmap(bitmap, 0, 0, paint);
                bitmap.recycle();
            }

            if (leftBottom != NO_TILE) {
                bitmap = BitmapFactory.decodeByteArray(leftBottom.data, 0, leftBottom.data.length);
                canvas.drawBitmap(bitmap, 0, 256, paint);
                bitmap.recycle();
            }
            if (rightTop != NO_TILE) {
                bitmap = BitmapFactory.decodeByteArray(rightTop.data, 0, rightTop.data.length);
                canvas.drawBitmap(bitmap, 256, 0, paint);
                bitmap.recycle();
            }
            if (rightBottom != NO_TILE) {
                bitmap = BitmapFactory.decodeByteArray(rightBottom.data, 0, rightBottom.data.length);
                canvas.drawBitmap(bitmap, 256, 256, paint);
                bitmap.recycle();
            }
            return true;
        }

        private Bitmap getNewBitmap() {
            Bitmap image = Bitmap.createBitmap(TILE_SIZE, TILE_SIZE,
                    Bitmap.Config.ARGB_8888);
            image.eraseColor(Color.TRANSPARENT);
            return image;
        }

        private static byte[] bitmapToByteArray(Bitmap bm) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 100, bos);

            byte[] data = bos.toByteArray();
            try {
                bos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return data;
        }
    }

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        googleMap.addTileOverlay(new TileOverlayOptions().tileProvider(new CanvasTileProvider(new UrlTileProvider(512, 512) {

            private String _token = null;
            private long _tokenExpireTime = 0;

            @Override
            public URL getTileUrl(int x, int y, int zoom) {
                String s = String.format(Locale.US, NEOTREKS_URL, zoom,y, x, getToken());

                URL url = null;
                try {
                    url = new URL(s);
                } catch (MalformedURLException e) {
                    throw new AssertionError(e);
                }
                return url;
            }

            private synchronized String getToken(){
                if (null != _token && _tokenExpireTime > new Date().getTime())
                {
                    return _token;
                }
                try {
                    //the short-lived token expiration time is 60 seconds
                    _tokenExpireTime = new Date().getTime() + 60000;
                    URL url = new URL(String.format("%s?request=getToken&username=%s&password=%s", NEOTREKS_TOKEN_URL, USERNAME, PASSWORD));
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("GET");
                    connection.setUseCaches(false);
                    connection.setDoInput(true);

                    //Get Response
                    InputStream in = connection.getInputStream();
                    int responseStatus = connection.getResponseCode();
                    if (responseStatus != 200) {
                        throw new Exception("Failed to generate token, server error");
                    }
                    BufferedReader rd = new BufferedReader(new InputStreamReader(in));
                    String line;
                    StringBuilder response = new StringBuilder();
                    if ((line = rd.readLine()) != null) {
                        response.append(line);
                    }
                    else {
                        throw new Exception("Failed to generate token");
                    }
                    rd.close();
                    //The output is the token
                    _token = response.toString();
                    Log.d("Generated new token", _token);
                } catch (Exception ex) {
                    _token = null;
                    Log.d("TOKEN", ex.toString());
                }
                return _token;
            }
        })));

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(30.533909d, -97.782507d), googleMap.getMaxZoomLevel()-6.5f));

    }
}
