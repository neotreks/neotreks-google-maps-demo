# README #

Sample application to show how to load NeoTreks map (ArcGIS) in Google Map View on Android. The sample contains tile provider that generates 512x512 tiles.

### What is this repository for? ###

* Sample application for NeoTrek's customers
* Initial version

### How do I get set up? ###

* Setup Google Api Key for Google Maps on Android.
* Change the key in app/src/debug/res/values/google_maps_api.xml (similar for release configuration)
* Enter your NeoTreks's user name and password in app/src/main/java/com/neotreks/googlemap/googlemap/MapsActivity.java

### License ###

* The code is provided for free use and copy as it is.

### Questions ###

* Contact NeoTreks only if you have any questions related to NeoTreks maps.